---
title: 'ITT2 Project'
subtitle: 'Project plan'
authors: ['Kenneth', 'Nicolai', 'Maxi']
main_author: 'KNM'
date: \today
email: 'x'
left-header: \today
right-header: Project plan
skip-toc: false
---



# Background

This is the semester project for ITT2 where we will combine different technologies to go from sensor to cloud. This project will cover the first part of the project, and will match topics that are taught in parallel classes.


# Purpose


The main goal is to have a system where sensor data is send to a cloud server, presented and collected, and data and control signal may be send back also. This is a leaning project and the primary objective is for the students to get a good understanding of the challenges involved in making such a system and to give them hands-on experience with the implementation also.

For simplicity, the goals stated wil evolve around the technical project goals. It must be read using the weekly plan as a complementary source for the *learning* oriented goals.


# Goals

The overall system that is going to be build looks as follows

![project_overview](project_overview.png)

Reading from the left to the right
* Analog input/output and digital input/output: These are sensor of different kinds that we are to implement into the system. Initially we will work with a temperature sensor.
* ATMega328: The embedded system to run the sensor software and to be the interface to the seralconnection to the raspberry pi.
* Raspberry Pi: Minimal linux system relevant programs to upload/download data from the ATMega and to/from the APIs.
* Juniper router: Router to protect your internal system from the untrusted networks, and to enable access to the Internet and the "cloud servers"
* Reverse proxy/API gateway: This is a server/router that collects and protects the API endpoints and webservers implemented by each group.
* webservers: There are one webserver per group. It will include the REST API implementation and the user interface website.


Project deliveries are:
* A system reading and writing data to and from the sensors/actuators
* A REST API exposing the values of the sensors and option of setting and applying the actuator values.
* A secure firewall enabling connections to the API.
* Docmentation of all parts of the solution
* Regular meeting with project stakeholders.
* Final evaluation

The project itself will be divided into 3 phases and gitlab will be used as the project management platform.


# Schedule

The project is divided into three phases from now to the easter holidays.

As the available time can fluctuate, consult the [lecture plan](https://ucl.itslearning.com/LearningToolElement/ViewLearningToolElement.aspx?LearningToolElementId=474120) for specific details.


# Organization

The project is organized to make use of the group member in a dynamic fashion 
as none of the group-members have a higher certification for any of the 
specific project parts.
This means that the Issues created will be assigned among the group members 
themselves.

## Members
* Kenneth Bjerg
* Nicolai Lyngs
* Maximilian Gregersen


# Budget and resources

No monetary resources are expected. In terms of manpower, only the people in 
the project group are expected to contribute.

The expected project-time for week 5 is determined at around 15 hours per person,
for a collective 45 hours.

| **Week Number** | **Expected workload in hrs** | **Collective time**         |
|-----------------|------------------------------|-----------------------------|
| 05              | 15hrs.                       | 45hrs.                      |
| 06              | 15hrs.                       | 45hrs.                      |
| 07              | 15hrs.                       | 45hrs.                      |
| 08              | 15hrs.                       | 45hrs.                      |
| 09              | 15hrs.                       | 45hrs.                      |
| 10              | 15hrs.                       | 45hrs.                      |
| 11              | 15hrs.                       | 45hrs.                      |
| 12              | 15hrs.                       | 45hrs.                      |
| 13              | 15hrs.                       | 45hrs.                      |
| 14              | 15hrs.                       | 45hrs.                      |

# Risk assessment
In the risk assessment section is where, we look at the different risk's in this
project. Here there will be a catagory for which the risk belong. There will be some
subjects of different problems, and then there will be some possible solutions, 
to those risk's.

**Documentation**<br />

Lack of documentation can be a risk. We need documentation so we can always, keep
track of how far we're in the progress, which kind of mistakes we have made. It's
also a good way to prove, how the progress is going to the stakeholders.

*Lack of teacher documentation:*<br />

The risk in not receiving the correct/needed documentation from the teacher, would
be that we go on a wrong track with the project. The documentation is therefore
crucial for the project to succeed.

*Solution:*

* *Communication with teachers at team meetings.* In the weekly team meeting with 
the teachers we must ensure, that we are on the right track, and if we're in 
doubt we should not hesitate to ask the teachers.
The details of said meeting will be noted in a document in the documentation folder
titled "YYMMDD_minutes_of_the_meeting". Consult the Documentation paragraph for 
more info.

*Lack of student documentation:*<br />

The lack of student documentation will mean, that we will be unable to
fix a problem, that we have fixed before. It could also be hard to keep track
of how far we're in the progress, who did what and who does what. Those are all 
risk's that could lead to seriously time waste.

*Solution:*

* *Step by step guide.* A step by step guide will be made if an Issue is found, 
which will ensure that we keep track of the progress that goes into the project,
and will ensure that we don't make the same mistakes that we might already have 
made.
This guide will then be uploaded on GitLab in the Resources folder located under
Documentation.

* *Keep a journal/log.* The journal will keep track of how far we're in the 
progress, it's also a good tool to see, if we work effecient enough. We will mainly
use gitlab's function to keep track of the process.

* *Design document.* The design documents are a crucial part of tracking how the
system is build/designed. The documents should keep track of curcuits and wiring,
so in case the system should be destroyed. It will be easier to rebuild it. And 
therefore minimalise the risk of time waste.
These can be found in the Designs folder located under Documentation.

* *List of tasks / milestones.* The list of tasks and milestones, are more or 
less the same as keeping a journal/log. Information on these tasks can be found
on GitLab's project Management resources. In specific, the canban board.

**Knowledge**<br />

To succeed in this project, a certain amount of knowledge is needed. This 
knowledge was primarily provided by attending first semester courses. This will
be the base knowledge of the project, more knowledge will be provided through
the second semester, and it is therefore crucial to attend second semester courses.

*Not knowing enough from last semester*<br />

If the student for some reason hasn't received the knowledge required from first
semester, the student might be a risk, delaying the workflow in the group. 
The student might be responsible for a big amount of timewaste.

*Solution*

* *Seek knowledge / help him.* The student should try to seek knowledge by attending
the courses provided in second semester. The student should also find out what
he is missing, and try to read up on it online. It is the group's responsibility
to try and help the student, by answering the students questions.

* *Be honest about what kind of knowledge you miss.* The only way to help the 
student, if is he is honest about what kind of knowledge he is lacking. The first
step on the road is to admit, how you can be better and improve.
This should be communicated to the group, either in a meeting, while working or
over one of the communication channels.

* *Group pressure to peform.* This might sound negative, but it's not. What this
mainly referer to, is that it's the groups resonsibility to make sure, that the 
student also gets some work to do. Instead of just doing it for him, because of
it's easier that way. This is the way the student actually learns to be better.

* *Reduce scope of project.* Incase none of the above work, it can always be a solution
to reduce the scope of the project. This may result in an all in all lower grade.
So this should be the last solution, if it however has to be done, a new scope should
be defined and written out so as to have everyone on the same page.

**Planning**<br />

Bad planning is one of the biggest risk of time waste. It may be the single course
of risk, that course the project to not be finished in time. Without good planning
comes alot of problems.

*Disorganization*<br />

Disorganization is one of the biggest risk in terms of time waste. It's also a 
way to keep running into the same problem over and over again. This is a risk, 
that should be taken very seriously into account.

*Solution:*

* *Issues + gitlab.* A good way to stay organized, is by using planning tools.
in this project we will use gitlab as our main tool. Gitlab is a great version 
control software. When things are uploaded to gitlab, it will be availbe for all
group members to access. Gitlab comes with project management tools, it have this
function called issues. Here the different task can be assigned. Plus issues got
a milestone function. It's possible to see who does what tasks.
 
* *Weekly meeting with teachers.* The weekly meeting with the teachers is there
to ensure, that we are always on track. This will help us if we're en doubt of 
anything. The teachers can also help if we use the tools wrong, and therefore need
to use them differently. For more info on that, consult the corresponding 
paragraph under communication.

* *SMART task.* SMART is a smart tool, which is really good to using when making 
tasks. When we create issue's in gitlab we should always think about if it is 
SMART, *Specific*, *Measurable*, *Achievable*, *Relevant* and *Time-bound*. 
If those criterias are fulfilled, then the task should be doable and
give the project value.

*Time waste*<br />

Without planning alot of time waste is going to take place. Time waste is a 
crucial risk, as it might be the course of not being finished before the deadline.

*Solution:*

* *SMART task.*  SMART is a smart tool (see what i did there?), which is really
good at using when making tasks. When we create issue's in gitlab we should always
think about if it is SMART, *Specific*, *Measurable*, *Achievable*, *Relevant* and 
*Time-bound*. If those criterias are fulfilled, then the task should be doable and
give the project value.

* *Do breaks to clear mind.* Always remember to take some short breaks now and 
then. It's better to take a break when not focusing, then to keep on doing something
only half. Rather take a break, clear your mind, and then continue fullspeed. this
is also a way to make the time you work more efficient.
As the amount of time one can work with / without a break is different from 
person to person, each group-member should take a break when it makes sense to them,
as well as not being an obstruction for the group.

**Communication**<br />

Communication is crucial for any project to succeed. Without prober communication
risk such as unclear task may apear. People might also start working on the same 
thing, and therefore just waste alot of time.

*Unclear tasks*<br />

Unclear tasks may result in people not knowing what to do, and therefor be a major
time waste. 

*Solution:*

* *Show up at meetings.* A way to always ensure, that you know what to do, is 
showing up to the meetings. This way you are always up to date, with the different
updates and task. It's also a good way to ask if you're in doubt of something.

* *Regular meetings.* There should be a meeting with the teachers each week. 
This way we ensure that we always knwo what to do. We will hold a short meeting
each day in the group. This way we ensure, that everyone know what to do, and if
anyone have a problem or anything, then we can talk about it. 

* *Talk nice- and clearly to each others.* A good way to ensure good 
communication is, to always talk well with each other. If a good tone is kept,
then no one will feel looked down upon. A good, clear and comfortable work 
enviroment, will also ensure good work.

**Motivation**<br />

Motivation is needed to complete the project. Motivation is the drive of the output
if people stay interested and motivated a good output will follow. If people however
is unmotivated, the entire project might be in risk of failing.

*Limited output*<br />

In lack of motivation a limited output of the project is in risk of happening. 
The limited output will be reflected on the grade, and the learning of the project.
The end user might not be satisfied with the product either.

*Solution:*

* *Adjust goal (02-12).* In case the motivation lack to much, and no one is doing
any work. Then the goal of the assignment can be adjusted. Instead of going for 
a grade 12, the goal should possibly be lowered as that will ensure, that the project
wouldn't need as much work.

* *Add man power.* In case of a missing group member over a longer period of time.
The solution can be, to add a new group member, and therefore come up to the 
regular man power. This will however need to be documented in the overview of 
group members in the Project Plan.

*Contagions (Negative-feedback loop) / Drop outs*<br />

This is one of the worst thing that can happen to a group. If either of these are 
happening the entire project is in risk of failing. It's therefore crucial to
get a solution ASAP.

*Solution:*

* *Team meetings.* A team meeting should be established ASAP! Here the agenda
will be about the persons lack of motivation / why he's not showing up. And a 
warning should be given, that if this happens again, the teachers will be involved.

* *Encouragement.* If it's only lack of motivation, then some encouragement might
help. Attempt to tell the person what they do well and give the member specific tasks 
which motivates them.

* *Kick them out (send them to teachers).* If it keeps repeating, or the person
does not show up, contact the teachers ASAP, and talk about the possiblities about
kicking the group member out, and either continue as a two man group, or get a
replacement.

**Electricial failuers** <br />

If there should be some last minute failure regarding the components, or just some
hardware failure all in all, the project is in risk of not being completed successfully.
Therefore is it a good idea to think about some solutions.

*Solution:*

* *Have a back-up.* Always remember to back-up. Every code and documentation 
should be on GitLab, so incase a computer gets destroyed, it will always be
possible to get the files from a different one. This is also why it's important
to have design documents, this way if some hardware fries, it is possible to make
them again. Which is also why we have a PI and Atmega each, in case one of them 
brakes, it can instantly be replaced while a replacement is ordered.

* *hope for the best.* In case it's a last minute breakdown, and no replacement
hardware is avaible. Try to explain what it does and will be able of doing as best
as possible, also tell the users when the prototype will be ready for show. 

# Stakeholders

Stakeholders are persons or organizations with interest in the project (and with active communication?).

**Teachers**

As the teachers have the responsibility of teaching the students throughout the duration of the project, their priority is the groups achievement of said project. Preferrably with as few hiccups along the way as possible.


**Other students**

With help from other students we can solve problems we can't fix ourselves and vice versa.

**The Group**

Every group member wants to reap the benefits of having completed the project and achieved the hereby gathered knowledge. Which will most likely not happen if the project is not completed.

**Wife's / family**

If the project doesn't go as planned we punish our families.

**End users**

TBD

**Random company** 

TBD. Potential internship.

> [An analysis of the stakeholder is conducted to establish who has an interest in the project. There will be multiple stakeholders with diverse interests in the project.
>
> Possible stakeholders
>     • Internal vs. external
>     • Positive vs. negative
>     • Active vs. passive
> A strategy could be planned on how to handle each stakeholder and how to handle stakeholders different (and/or conflicting) interests and priorities.
> This could also include actions designed to transform a person or a group into a (positive) stakeholder, or increase the value of the project for a given stakeholder.]
>
> TBD: This section is to be completed by the students.
> TBD: Please consider finding a relevant external stakeholder.

# Communication
This section will explain the way, we're going to communicate towards the 
stakeholder. It will explain in which way the communication are going to take 
place. I.e. which channel is used and how often communication take place. 

**Teachers**

The communication between the group will primarily take place, with the weekly
meeting between the group members, and the teacher staff of the project. If we
e.g. should run into an issue, that needs to be solve before next meeting, we 
we will contact the teacher by mail. It's important to keep a weekly communication
with the teachers, as they're the ones providing us with the task and knowledge
to succeed in this project.  

| **Teacher's Name** | **GitLab Name** | **Mail**    |
|--------------------|-----------------|-------------|
|Morten Nielsen      | @moozer         | mon@ucl.dk  |
|Nikolaj Simonsen    | @npes           | nisi@ucl.dk |

**Other students**

When new ideas or knowledge is needed, we will strive to get that kind of information
from our fellow students. The way this is done is by mouth to mouth communication,
plus information sharing. We will get a good relationship with our fellow 
students, by participating in the different classes, and also spend some 
free time with them, e.g. the friday bar. It's always good to share knowledge
between the different groups.

**The group**

One of the keygoals for the project to succeed is the intern communication in the
group. We will have a weekly group meeting, where we will make sure that everything
is up to date, that the milestones are completed, and that no-one is stuck on anything.
We will primarily talk when we are with each other. Else a group Discord server
will work as the main communication platform. Every part of documentation and
track of progress will be on gitlab.

**Contact information**

|**Name**|**Discord Alias**|**Mail**|
|--------|-----------------| -------|
|Nicolai      | Koldbaek#7375      | nico429s@edu.eal.dk|
|Maximilian   | TheOnlyFP#1780     | maxi0112@edu.eal.dk|
|Kenneth      | Huzo#1960          | kenn5046@edu.eal.dk|

**Wive's/family**

The wive's and family are the ones suffering from when we have to work overtime.
It is therefore crucial to keep the loved ones at home, up to date on how the project
is going. It should be told to them in good time, if we will be home late, so they
don't get dissapointed and let hell rain upon us. If things are good at home,
then there are more mental resourcess to focus on the project. The old saying
are ones again true: "Happy wife, happy life".

**End users**

The reason why the communication between us and the end user are important. Is
because of the end user are the ones that are going to use the product. It is 
therefore important to keep a good and stable communication between us, so the
end user's need can be fulfilled. It is also good, if we need testing with the 
product, then we can use the end user, which we're already in contact with.
As the end users are not yet determined, the channel for which the communication
will take place are not determined either. 

**Random company**

N/A. As the company have not yet be determined, it is not possible to write, anything about it.

# Perspectives

This project will serve as a template for other similar project, ike the second one in ITT2 or in 3rd semester.

It can also serve as an example of the students abilities in their portfolio, when applying for jobs or internships.


# Evaluation

- 5 randomly selected People with no prior knowledge or experience towards the project should be capable of utilizing 
it's functionality, meaning that they should be able to acces the data on the webserver directly and understand
what it means, i.e. usablility should be good enough for easy use. 
The testing-method will be defined in the corresponding document and will be concluded in week 13 at latest. 

- Every group member shall be capable of explaining the system as a whole and when asked, each component's 
functionality in the system in broad strokes as well as the reasoning behind it's implementation.


# References

"None at this time"
