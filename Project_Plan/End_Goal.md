# End-Goal
The finished product of this project has been determined as sensor-based
plant-growth-enviroment observation tool.



## Measureables
<b> Humidity </b>
As different plants need different humidities for optimal growth, observing
the humidity of its close enviroment is of utmost importance. On top of this
humidity is the biggest factor in transpiration, meaning that to low a 
humidiy will force the plant to evaporate more than it can handle, while a
humidity to high will make the uptake of nutrients to slow, which can lead
to stunted growth or even a death of the plant.
This will be measured with a humidity sensor.

<b> Temperature </b>
As plants are accustumed to different climates it is highly benefical to
ensure that a fitting temperature is met as a temperature to cold might 
stunt the growth of a plant, whereas a temperature too cold might overheat
or speed up the evaporation of the plant's parts.
This will be measured with a temperature sensor.

<b> Sunlight </b>
Unbeknownst to few a plant needs some variation of light to grow. 
Therefor it is important to keep an eye on if the plant recieves
the light it needs to grow, even when one isn't home to observe whether or
not the light reaches the plant.


## Components

| Component      | Use                          | Name |
|----------------|------------------------------|------|
| Thermsistor    | Measuring the Temperature    |      |
| Photo-resistor | Measuring the light/sunshine |      |
| "Humidity-sensor"| Measuring the Humidity     |      |



## Reasoning
In the hectic city-life, there often is a need for some sort of hobby.
Many choose to make plant-growth a hobby, but for many growning plants is 
just to dificcult. 

As humans we can make connections to most if not all mammals, read their
emotions and mental state and if not properly, many can & will tell us.
But a plant can't speak. That is why it is important to keep an eye on it 
if it's growth is to succeed. But even when one knows the what is neccessay 
to make any given plant grow optimally, they may not have all the tools
or know how to use them. 

This system will cater to exactly that need. All the data always available.
Is the dirt of the tomatoes moist when going to work and moist when coming
home? There is no ensuring that it was moist the whole day however.
If one knows that something has happend to their beloved plant. They will
be able to act upon it. If not, it might just wither away silently under
failing caretaking. 