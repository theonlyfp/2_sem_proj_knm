# ITT2_project_KNM

Second Semester Project. 


[__Guide for the Project__](Documentation/Raspberry_PI/Raspberry_PI_setup.md)

[__Dashboard files__](https://gitlab.com/NicolaiLyngs/19s-itt2-dashboard-example?nav_source=navbar)


**Contact information**

|**Name**|**Discord Alias**|**Mail**|
|-----|:--------:|:--------:|
|Nicolai      | Koldbaek#7375      | nico429s@edu.eal.dk|
|Maximilian   | TheOnlyFP#1780    | maxi0112@edu.eal.dk|
|Kenneth      | Huzo#1960         | kenn5046@edu.eal.dk|