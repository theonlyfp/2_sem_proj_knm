# LED status colour-codes 

The LEDs talked about in this document are the 4 programmable LEDs located. These show different status-codes depending on the current status.  


| Available Colours | B | G | R |
|-------------------|---|---|---|
|Blue  | 11111111 | 00000000 | 00000000 |
|Green | 00000000 | 11111111 | 00000000 |
|Red   | 00000000 | 00000000 | 11111111 |
|Purple | 11111111 | 00000000 | 11111111 |
|Orange | 00000000 | 00001111 | 11111111 |
|Black | 00000000 | 00000000 | 00000000 |



| Status | LED1 | LED2 | LED3 | LED4 | State | Time | Description |
|--------|------|------|------|------|-------|------|-------------|
| Boot-up | Green | Green | Green | Green |  Turns on one by one depending on location in the boot-up sequence  | Until done | Shows how far in the boot-up sequence the system is. |
| Ready-state | Green | Green | Green | Green | All on | Until first command is executed | The standard start-state, showing that the system is ready for use |
| Finished UART send | Blue | Blue | Blue | Blue | All on | Until next command | Shows that UART has been sendt sucessfuly |
| Recieved UART | Blue | Purple | Purple | Blue |  All on | Until next command | UART has been successfuly recieved |
| Failed UART | Blue | Red | Red | Blue | All on | Until next command | Atmega did not return expected value |
| Turned off | Black | Black | Black | Black | All off | Until turned on | System is not active |