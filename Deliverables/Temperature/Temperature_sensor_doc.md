# Delivarable: Temperature Sensor on board

The deliverable was to build a board with a Thermsistor, Button & an LED.

The base-schematic was taken from [this link](https://gitlab.com/atmega328-sensor-sample/schematics-orcad).

## Components

- LED (Green)
- Button / H-Bride
- Resistor (120 ohm)
- Thermsistor (TMP36GT9Z) 
- Stripboard


## The assignment 
In this exercise a verobard with a temperature sensor, a button and an led had to be build.
The board will be connected to GPIO pins on an atmega328 the use a 5V supply voltage.

## Finding the Resistor value
To find the resistor's value, the preffered current was decided as 20mA.
Then the wanted Voltage across the LED was decided to 2.5V as this was more than enough for the LED to light up but not close to any potential maximal Voltage of it. 
This ment that that the voltage drop across the Resistor had to be:
5V - 2.5V = 2.5
As the Voltage across the Resistor is 2.5V and the desired current was 20mA (0.02A), Ohm's Law was used.
V / I = R ; 2.5V / 0.02 = 125 ohm
Therefor the optimal resistor for our wanted circuit was determined to be 125 ohm.
However as the closest available resistor was 120 ohm. That was used instead.
Leading the circuit to look like this:

![OrCad circuit](Temp_sensor_circuit.PNG)

## Stripboard Layout
Following the calculation, the bord-layout was designed in Fritzing.

![Fritzing Schematic](fritz_tempsens.png)

## Construction
The construction of the circuit was done on a stripboard following the layout created beforehand, soldering all the components into the correct position.

![Finished Circuit](pin_connection.png)


## Testing the LED
LED-testing was done by connetion +5V and ground to the corresponding pins of the circuit. As can be seen below, the LED lit up.

![LED lighting up](temp_sensor_LED_on.jpg)

## Button testing
The Button was tested by connecting the +5V to it and the LED. When the Button is pressed, the Voltage should go through the button and therefor turn off the LED, proving that the button connects to ground.

![Button works](temp_sensor_LED_off.jpg)

## Thermsistor testing
To test the thermsistor [this](https://learn.adafruit.com/tmp36-temperature-sensor/testing-a-temp-sensor) guide was used. However it wasn't tested with temperatures colder than room-temperature. 

**Room-Temperature**

![Room temp thermsistor](temp_sensor_0.76V.jpg)

As can be seen the Room temperature sets the resistor to allow 0.76V

**Finger-warmth**

![Finger warmth resistor](temp_sensor_0.801V.jpg)

As can be seen above, the Finger Warmth sets the resistor to allow 0.801V


**Conclusion**

This means that the resistor allows more Voltage through as it gets warmer.


## Authors

- Nicolai Lyngs Jensen
- Kenneth Bjerg Enevoldsen
- Maximilian Wassmann Gregersen