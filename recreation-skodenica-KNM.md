Recreation checklist for group skodenica
===================================================

Group name: KNM

Checklist
-----------
 
- [x] Project plan completed
- [ ] Minimal circuit schematic completed
- [ ] ATmega328 minimal system code with tempsensor board workig
- [ ] Atmega328 to Raspberry Pi serial communication over UART working
- [ ] Raspberry pi configuration week06 requirements fulfilled
- [ ] Networked raspberry pi+atmega system behind a firewall accessible using SSH
- [ ] Temperature readout in celcius from the temperature sensor
- [ ] Raspberry API Server is able to serve temperature readings
- [ ] Virtual server installed
- [x] `group.md` complete according to specification
- [ ] Dashbaord communicating with API


Comments
-----------

<Any comments that may be relevant for you, other groups or teachers. (Could be that something is super succesful)>