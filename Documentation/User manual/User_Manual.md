# Set-up

Before use of the system, simply plug in the charger into a wall plug or power 
connector and ethernet cable into the configured router port.
Then wait for the start up to finish by looking for the four status LEDs to
shine green.

# Using the system

To us the system, simply connect to the configured web-page.

# Available functionality

As of now the web-interface is cabable of turning the LED on & off by using
the buttons in the top-left and displaying the temperature measured, on the 
graph.

# Status LED code table

- [] insert LED status code table her plz


# Error handeling

If at any point the system stops working, disconnect and re-connect the charger 
to re-boot the system forcefully.