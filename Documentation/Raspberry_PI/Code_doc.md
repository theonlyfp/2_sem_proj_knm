# Raspberry Pi code

## Table of Contents

- [atmega.py](#atmega-py)
- [atmega_me_in_u.py](#atmega_me_in_u-py)
- [flask_menu.py](#flask_menu-py)



## atmega.py 
This file holds all the function for communicating with the atmega, including the
adc-value to temperature conversion function named "adc_con_func".
Each function except for the conversion function is named following their 
functionality.

The communication is done by sending a specific string (depending on the function)
to the atmega over the parsed serial connection.

| *Function-name* | *Function* |
|-----------------|------------|
| read_LED        | Reads the LED-status from the atmega |
| read_temp       | Reads the temperature measured by the thermsistor, adc-value through the atmega |
| adc_con_func    | Converts an adc-value into Celcius |
| read_button     | Reads if the button has been pressed since last check |
| turn_on_LED     | Turns on LED on atmega |
| turn_off_LED    | Turns off LED on atmega |
| blink_LED       | Blinks LED on atmega in an interval |
| exit_program    | Exits the script |


## atmega_me_in_u.py
This file holds the menu builder, which writes out the dictionary of 
[atmega.py's](#atmega-py) functions in a simple menu by parsing keys to the 
dict_funct_to_name function, which returns the name of a function stored in the
dictionary and printing the key followed by the name of the function.

An input is taken and parsed to the choice_taker function which parses the 
serial connection to the function specified via the key. It does however re-print
the menu if the input is empty. 

| *Function-name* | *Function* |
|-----------------|------------|
| exit_program    | Prints a message and executes the "exit" function of python |
| dict_func_to_name | Pulls the value out of a dictionary, in this case the name of a function. |
| menu_builder | Prints a simple menu based on the function dictionary |
| choice_taker | Parses the serial_cfg to the chosen function via the dictionary |

## flask_menu.py
The flask menu holds the api for the pi-server built using the flask module.
It makes use of the functions already included in the [atmega script](#atmega-py) and display's the data in a
json format.
The first page "/" simply shows the LED & Button state together with the temperature 
measured by the Atmega.


| *URL* | *Function* |
|-----------------|------------|
| /   | Displays the atmega's Button-state & Temperature |
| /TEMPERATURE | Returns the temperature measured by the atmega's sensor |
| /< led > | Choses whitch LED will be turned on via the key-value pair below (only led1 supported for now). |

To change the state of the LED, a put-request has to be made, these can be done like following.

| *Key* | *Value* | Function |
|-------|---------|--------------|
| state | on | Turns the LED on |
| state | off | Turns the LED off |

