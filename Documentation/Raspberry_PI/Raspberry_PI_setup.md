# Raspberry PI installation

## Starting Requirements
The installation will start from a clean version of [raspian-stretch-lite](https://www.raspberrypi.org/downloads/raspbian/).
It's important that the raspberry have internet access, as this is needed to get the reinstallation script, and also downloading
crucial part for the reinstallation.

## SSH-keys:
The SSH-keys will be downloaded to the pi using a curl from "https://gitlab.com/$User.keys" 
using a predefined list of gitlab usernames.

## Setup
The Pi is set up by running the "hello.sh" script via "sudo ./hello.sh"

To gain access to the Raspberry pi, please add your Gitlab.com usernames to the hello.sh script on line
13 in the brackets of Allowed users. 

Like this:

``Allowed_users=($User1 $User2 $User3)``

Before the ./hello.sh script can be run, it must be changed to an executable file.
When in the same directory as the hello.sh file enter this:

``chmod 755 hello.sh``

The reinstallation script can now be run. 

``sudo ./hello.sh``

Please note that the Raspberry will be setup with a static IP Address. The static
IP will be **172.20.10.3** and it will be on the ethernet port: eth0.

## Connections
The Raspberry and Atmega are connected like this:

![Pi to Atmega](pitoat.jpg)

While the Sensor-board with the temperature-sensor has to be connected as this:

![Pin Connection on Sensor board](pin_connection.png)

The Schematic for which is:

![Sensor board](Temp_sensor_circuit.PNG)

## Running the code
Login on the PI using the user "pi", as the SSH-keys have been added to it.
When everything is connected & installed, the flask menu can be found under /home/pi/2_sem_project_knm/Flask_api.

Check to see if it is already running by writing `` sudo systemctl status flask_api.service `` 
If not: run it manually by writing: ``python3 /pi/2_sem_project_knm/Flask_api/flask_menu.py``

## Api-functionalities
To see what can be done via http console the [code_doc here](Code_doc.md)
It is recommended to test the api via [Postman](https://www.getpostman.com/) if no Dashboard is available.

# Atmega installation

The Atmega has to be flashed using the atmel studio project found [__here__](https://gitlab.com/theonlyfp/2_sem_proj_knm/tree/master/Atmega_code/DataLogger) 
or alternativly under ``/2_sem_project/Atmega_code/DataLogger``.

# Router config
The router config can be found [__here__](https://gitlab.com/theonlyfp/2_sem_proj_knm/tree/master/Router). This config will have to be uploaded to the router.
The Raspberry is to be connected to ge-0/0/2.0 .
Remember to add the own SSH-keys to the configuration, if ssh access is wanted.