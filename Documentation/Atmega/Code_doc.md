# Atmega328P code / Datalogger.atsln

## Table of Contents

- [APA_SPI](#APA_SPI)
- [main.c](#main.c)
- [include_file_1.h](#include_file_1.h)



## APA_SPI.c
This file holds all the functions for communicating with & setting up the APA102 SPI addressable LEDs.

**Important:** The LED_strip_array stores the values for both Status LEDs [0-3] and Plant_LEDs [4-"Number of Plant-LEDs defined"], 
meaing that it is changed whenever status-LEDs or Plant_LEDs need to be updated.

The communication is done by sending the data-frame specified by the documentation over SPI.

| *Function-name* | *Function* |
|-----------------|------------|
| setup_LED_strip_array | Populates the array used for storing wanted LED-values with 0s |
| setup_SPI       | Enables SPI communication at the hardware level on the Atmeag328P's dedicated pins |
| send_startframe_SPI    | Sends the start-frame specified by the APA102 docs |
| send_endframe_SPI     | Sends the end-frame specified by the APA102 docs |
| send_colour_SPI     | Sends a data-package to the LED with the colour-values parsed to it |
| LED_update    | Updates all LEDs configured with the values stored in the LED_strip_array |
| startup_Led       | Runs the startup routine on the Status-LEDs step by step for each time it is called up till 5 times before it starts from 0 |
| Plant_LED_colour_change    | Changes the values for the Plan-LEDs in the LED_strip_array according to the parsed values |
| successful_UART_LED | Changes the Status LEDs to the colour speciefied for it in the status docs |
| failed_UART_LED | Changes the Status LEDs to the colour speciefied for it in the status docs |
| recieved_UART_LED | Changes the Status LEDs to the colour speciefied for it in the status docs |
| reset_Status_LED | Resets the Status-LED values in the LED_strip_array |
| update_status_LEDs | Updates the LED_strip_array status-LED values with the values from the status variables |


## main.c
This file holds the main function of the code and the basic setup and functions.

| *Function-name* | *Function* |
|-----------------|------------|
| main | Runs the set-up functions, polls the UART-flag and reads the input to call the corresponding functions |
| ISR(TIMER0_OVF_vect) | Tests if the button is pressed |
| ISR(USART_RX_vect) | Sets the UART-flag |
| setupTimer0 | Sets up timer 0 and allows it to interrupt |
| initPort | Initializes ports according to what's parsed to it |
| enablePullup | Enables the pull-up resistor on the ports parsed to it |
| readLed | Reads the state of the port parsed to it |
| ledOn | turns on the PortB parsed to it |
| ledOff | turns off the PortB parsed to it |
| setupADC | Initializes the ADCin the Atmega with a 128 prescaler on the parsed port |
| startConversion | Has the ADC read the value and return it |


## include_file_1.h
This file holds the Protoypes, libary includes, Macros and Variabel declerations. 
The only thing that should ever be necessary to change is the LED_count macro 
as it defines how many LEDs are used, of which the first 4 are reserved for Status-code.

