## USER:
Nicklas
Khartiban
Henning
Morten
Tihamer


## TEST Plan Results:
Boot-up sequence gets stuck when Dashboard is turned on


## FEEDBACK:
Button not used
LED is pink not purple.
Specify that the user should be connected to EAL-wireless, else he won't be able to connect 
Specify in the user manual what the graph is for, (extern sensorboard) 
The button is useless 
It's a bit confusing with UART, what does it mean? 


## INTERACTION
User might pull out the cables
User attempted to click "led1" below the buttons


## Fixes
- [ ] Swap UART in guide with "internal communication"
- [ ] Add to user-guide that Dashboard should be closed on boot-up of the system
- [ ] Change LED-colour from "purple" to green
- [ ] Specify that the network to connect to has to be EAL-wireless
- [ ] Specify in user-guide that the temperature comes from the black component on the sensor-board
- [ ] Add ethernet cable re-plug to error-handeling