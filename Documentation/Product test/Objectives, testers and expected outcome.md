### Objectives

- Test the minimal system and observe if it performs as intended.
- Present the user manual and the product to the user 
- Can users figure out how to use the product?
- *Would* they actually want to use the product  — and if not, why not?
- Observe the user and ask whether it is understandable and useful

### Testers defined

- Who is the users? 
  - Classmates / colleagues / Teachers
- Pitch the product for users! (before users perform tests, you should explain the purpose of the product)
  - It can measure temperature and blink control a LED... 
- Define a test plan to ensure uniformity between test (What tests are users going to perform?)
  - Follow *Quick start*
    - Does the user understand what happens and what is shown?
  - Use dashboard. 
    - Does the user understand what happens and what is shown?

### Expected outcomes

- What do you want to achieve with the product test? (Confirm intended use? Get awareness of issues?)
  - General feedback about the product - see objectives.
- How are you going to document results? (How will you collect results on test day? How will you compare results?)
  - Video record if allowed by the user.
  - Take notes about how the user interacts with product.
  - Ask them if there are possible changes they would like to see happen to the product.
- How are you going to use the results? (correct them in the project?)
  - Results will be discussed among the team and improvements will be implemented using the available data.