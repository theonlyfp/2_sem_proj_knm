# Minutes of the meeting with teachers.

### Agenda:

**Status on project**<br />

We're where we should be

**Next steps** <br />

Maximilian is going to the fablab course. Kenneth and Nicolai will work with this weeks exercises.
The dashboard should be done with temprature reading. 

**Collaboration within the group** <br />

We're working well together. 

**Help needed or offered** <br /> 

None at this moment.

**Any other business (AOB)** <br />

None at this moment.
