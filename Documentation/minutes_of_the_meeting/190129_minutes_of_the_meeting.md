# Minutes of the meeting with teachers.

### Agenda:

**Status on project**<br />

We're in a good spot with the project. We have created task for the most part, of
the first 1 phase in step 1. We are over halfway done with fine tuning the project
plan, and therefore the well elabroated version is soon done.
    
**Next steps** <br />

We're going to finish the first draft of the project plan before monday(04/02). We will add contact
information in the project plan before today ends. We will continue to add more
task. And we're going to build a temperature sensor to day.

**Collaboration within the group** <br />

We're dividing the work evenly at this moment. We first started the project, but
this far the teamwork seems good. We have a fine balance between fun and hard
efford.

**Help needed or offered**<br /> 

We're going to present our project plan for the class, this monday(04/02). We
feel in a comfortable spot with our project plan, and therefore, we would like to
give our comrades inspiration on how to make it. We will make an issue for comments
on the project plan, and add Morten (moozer) to this issue, so he will provide us
with some comments before monday.