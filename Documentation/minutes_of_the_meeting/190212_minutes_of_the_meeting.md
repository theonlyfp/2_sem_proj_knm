# Minutes of the meeting with teachers.

### Agenda:

**Status on project**<br />

We feel that we're at a really good spot. 
    
**Next steps** <br />

We have to finally decide, what our project is going to be about. We do have
some ideas. We have to make a PP with the result. We need to make the reinstall
script for the Pi. We need to get physical router up and running.

**Collaboration within the group** <br />

Max did a good job with exercise 3 and 4. We work well withing the group.

**Help needed or offered**<br /> 

We need an IP from the teacher. We need some nice inspiration for the script
making.

