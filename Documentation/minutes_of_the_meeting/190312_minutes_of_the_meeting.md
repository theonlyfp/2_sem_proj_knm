# Minutes of the meeting with teachers.

### Agenda:

**Status on project**<br />

We're where we should be

**Next steps** <br />

Maximilian is working on coding the LED's. Kenneth is working on the design of the housing. Nicolai is working on adding the temp sensor to the dashboard.

**Collaboration within the group** <br />

We're working well together. 

**Evaluation of test result from ww10** <br />

The group we got assigned, haven't really made anything. The only two things we could cross of the list, was the "group.md" and the "project plan". We got a few bugs on our project, which we have solved. 

**Help needed or offered** <br /> 

None at this moment.

**Any other business (AOB)** <br />

We need to get some components. 