# Minutes of the meeting with teachers.

### Agenda:

**Status on project**<br />

We are feeling a bit pressure, as our user manuel isen't done yet. We haven't define the expected outcome of the product testing yet.
We just switched from using an atmega mini xplained, to just using a microchip.

**Next steps** <br />

We have to finish the things which we haven't done yet, see "Status on project" above.

**Collaboration within the group** <br />

We work well within the group. No drama. 

**Help needed or offered** <br /> 

None at this moment. 

**Any other business (AOB)** <br />

None at this moment.
