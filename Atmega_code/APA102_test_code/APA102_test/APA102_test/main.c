/*
 * GccApplication9.c
 *
 * Created: 10/03/2019 12.19.58
 * Author : FP
 */ 

#define F_CPU 16000000UL

#include <avr/io.h>
#include <util/delay.h>

void setup_SPI(void);
void send_colour_SPI(char,char,char,char,int);
void send_startframe_SPI(void);
void send_endframe_SPI(void);
void startup_Led(void);

char Blue_value;
char Green_value;
char Red_value;
char Brightness;
int LED_amount;

int boot_counter;

int main(void)
{
	boot_counter = 0;
	setup_SPI();
	Brightness = 0b11100001;
	LED_amount = 8;
	while (1)
	{
		startup_Led();
		_delay_ms(200);
		startup_Led();
		_delay_ms(200);
		startup_Led();
		_delay_ms(200);
		startup_Led();
		_delay_ms(200);
		startup_Led();
		_delay_ms(500);
		boot_counter = 0;
		
	}
}

void setup_SPI(void)
{
	PORTB |= (1<<PORTB1);
	// Sets MOSI & SCK / clock as outputs
	DDRB = (1<<DDB3) | (1<<DDB5) | (1<<DDB1);
	// Enable SPI 
	SPCR = (1<<SPE) | (1<<MSTR) | (1<<SPR0);
}

void send_startframe_SPI(void)
{
	for (int i = 0; i<4; i++)
	{
		SPDR = 0b00000000;
		while(!(SPSR & (1<<SPIF)));
	}
}

void send_endframe_SPI(void)
{
	// End Frame (Makes the last LED follow the others, if not it will be 1 cycle behind)
	for(int i = 0; i<4; i++)
	{
		SPDR = 0b11111111;
		while(!(SPSR & (1<<SPIF)));
	}
}

void send_colour_SPI(char Blue_value, char Green_value, char Red_value, char Brightness, int LED_amount)
{	
	for (int i = 0; i<LED_amount; i++) //updates LEDs
	{
		//Packet-start bits (first 3) + Brightness/Global (last 5)
		SPDR = Brightness;
		while(!(SPSR & (1<<SPIF)));
		//BLUE
		SPDR = Blue_value;
		while(!(SPSR & (1<<SPIF)));
		//GREEN
		SPDR = Green_value;
		while(!(SPSR & (1<<SPIF)));
		//RED
		SPDR = Red_value;
		while(!(SPSR & (1<<SPIF)));
	}
}


void startup_Led(void)
{
	switch(boot_counter)
	{
		case 0 :
			send_startframe_SPI();
			send_colour_SPI(Blue_value = 0b00000000, Green_value = 0b00000000, Red_value = 0b00000000, Brightness, LED_amount = 6);
			send_endframe_SPI();
			boot_counter++;
			break;
		case 1 :	
			send_startframe_SPI();
			send_colour_SPI(Blue_value = 0b00000000, Green_value = 0b11111111, Red_value = 0b00000000, Brightness, LED_amount = 1);
			send_colour_SPI(Blue_value = 0b00000000, Green_value = 0b00000000, Red_value = 0b00000000, Brightness, LED_amount = 5);
			send_endframe_SPI();
			boot_counter++;
			break;
		case 2 :
			send_startframe_SPI();
			send_colour_SPI(Blue_value = 0b00000000, Green_value = 0b11111111, Red_value = 0b00000000, Brightness, LED_amount = 2);
			send_colour_SPI(Blue_value = 0b00000000, Green_value = 0b00000000, Red_value = 0b00000000, Brightness, LED_amount = 4);
			send_endframe_SPI();
			boot_counter++;
			break;
		case 3 :
			send_startframe_SPI();
			send_colour_SPI(Blue_value = 0b00000000, Green_value = 0b11111111, Red_value = 0b00000000, Brightness, LED_amount = 3);
			send_colour_SPI(Blue_value = 0b00000000, Green_value = 0b00000000, Red_value = 0b00000000, Brightness, LED_amount = 3);
			send_endframe_SPI();
			boot_counter++;
			break;
		case 4 :
			send_startframe_SPI();
			send_colour_SPI(Blue_value = 0b00000000, Green_value = 0b11111111, Red_value = 0b00000000, Brightness, LED_amount = 6);
			send_endframe_SPI();
			boot_counter++;
			break;
	}
}