/*
 * GccApplication9.c
 *
 * Created: 10/03/2019 12.19.58
 * Author : FP
 */ 

#include "Include_file_1.h"
#include <avr/io.h>

void setup_LED_strip_array(void)
{
	for (int i = 0; i<4; i++)
	{
		LED_single_array[i] = 0b00000000;
	}
	for (int i = 0; i<LED_amount; i++)
	{
		for (int n = 0; n<4; n++)
		{
			LED_strip_array[i][n] = LED_single_array[n];
		}
	}
}

void setup_SPI(void)
{
	PORTB |= (1<<PORTB1);
	// Sets MOSI & SCK / clock as outputs
	DDRB = (1<<DDB2) | (1<<DDB3) | (1<<DDB5);
	// Enable SPI 
	SPCR = (1<<SPE) | (1<<MSTR) | (1<<SPR0);
}

void send_startframe_SPI(void)
{
	for (int i = 0; i<4; i++)
	{
		SPDR = 0b00000000;
		while(!(SPSR & (1<<SPIF)));
	}
}

void send_endframe_SPI(void)
{
	// End Frame (Makes the last LED follow the others, if not it will be 1 cycle behind)
	for(int i = 0; i<4; i++)
	{
		SPDR = 0b11111111;
		while(!(SPSR & (1<<SPIF)));
	}
}

void send_colour_SPI(char Blue_value, char Green_value, char Red_value, char Brightness, int LED_amount)
{	
	for (int i = 0; i<LED_amount; i++) //updates LEDs
	{
		//Packet-start bits (first 3) + Brightness/Global (last 5)
		SPDR = Brightness;
		while(!(SPSR & (1<<SPIF)));
		//BLUE
		SPDR = Blue_value;
		while(!(SPSR & (1<<SPIF)));
		//GREEN
		SPDR = Green_value;
		while(!(SPSR & (1<<SPIF)));
		//RED
		SPDR = Red_value;
		while(!(SPSR & (1<<SPIF)));
	}
}

void LED_update(void)
{
	send_startframe_SPI();
	for (int x = 0; x<LED_count; x++)
	{
		send_colour_SPI(LED_strip_array[x][1],LED_strip_array[x][2],LED_strip_array[x][3],LED_strip_array[x][0], 1);
	}
	send_endframe_SPI();
}

void startup_Led(void)
{
	switch(boot_counter)
	{
		case 0 :
			for (int i=0; i<LED_count; i++)
			{
				LED_strip_array[i][0]= 0b11100001; //Brightness
				LED_strip_array[i][1]= 0b00000000; //Blue
				LED_strip_array[i][2]= 0b00000000; //Green
				LED_strip_array[i][3]= 0b00000000; //Red
			}
			LED_update();
			boot_counter++;
			break;
		case 1 :	
			for (int i=0; i<boot_counter; i++)
			{
				LED_strip_array[i][0]= 0b11100001; //Brightness
				LED_strip_array[i][1]= 0b00000000; //Blue
				LED_strip_array[i][2]= 0b11111111; //Green
				LED_strip_array[i][3]= 0b00000000; //Red
			}
			LED_update();
			boot_counter++;
			break;
		case 2 :
			for (int i=0; i<boot_counter; i++)
			{
				LED_strip_array[i][0]= 0b11100001; //Brightness
				LED_strip_array[i][1]= 0b00000000; //Blue
				LED_strip_array[i][2]= 0b11111111; //Green
				LED_strip_array[i][3]= 0b00000000; //Red
			}
			LED_update();
			boot_counter++;
			break;
		case 3 :
			for (int i=0; i<boot_counter; i++)
			{
				LED_strip_array[i][0]= 0b11100001; //Brightness
				LED_strip_array[i][1]= 0b00000000; //Blue
				LED_strip_array[i][2]= 0b11111111; //Green
				LED_strip_array[i][3]= 0b00000000; //Red
			}
			LED_update();
			boot_counter++;
			break;
		case 4 :
			for (int i=0; i<boot_counter; i++)
			{
				LED_strip_array[i][0]= 0b11100001; //Brightness
				LED_strip_array[i][1]= 0b00000000; //Blue
				LED_strip_array[i][2]= 0b11111111; //Green
				LED_strip_array[i][3]= 0b00000000; //Red
			}
			LED_update();
			boot_counter = 0;
			break;
	}
}

void Plant_LED_colour_change(char Blue_value, char Green_value, char Red_value)
{
	for (int i = 4; i<LED_count; i++)
	{
		LED_strip_array[i][0] = Brightness;
		LED_strip_array[i][1] = Blue_value;
		LED_strip_array[i][2] = Green_value;
		LED_strip_array[i][3] = Red_value;
	}
}

void successful_UART_LED(void)
{ 
	reset_Status_LED();
	
	for (int i = 0; i<4; i++)
	{
		Status_Brightness[i] = 0b11100001;
	}
	for (int i = 0; i<4; i++)
	{
		Status_Blue[i] = 0b11111111;
	}
	
	update_status_LEDs();
}

void failed_UART_LED(void)
{	
	reset_Status_LED();
	
	for (int i = 0; i<4; i++)
	{
		Status_Brightness[i] = 0b11100001;
	}
	
	Status_Blue[0] = 0b11111111;
	Status_Red[1] = 0b11111111;
	Status_Red[2] = 0b11111111;
	Status_Blue[3] = 0b11111111;
	
	update_status_LEDs();
	
}

void recieved_UART_LED(void)
{	
	reset_Status_LED();
	
	for (int i = 0; i<4; i++)
	{
		Status_Brightness[i] = 0b11100001;
	}
	
	Status_Blue[0] = 0b11111111;
	Status_Green[1] = 0b00111111;
	Status_Green[2] = 0b00111111;
	Status_Blue[3] = 0b11111111;
	
	update_status_LEDs();
	
}

void reset_Status_LED(void)
{
	for (int i = 0; i<4; i++)
	{
		Status_Blue[i] = 0b00000000;
	}
	for (int i = 0; i<4; i++)
	{
		Status_Green[i] = 0b00000000;
	}
	for (int i = 0; i<4; i++)
	{
		Status_Red[i] = 0b00000000;
	}
}

void update_status_LEDs(void)
{	
	send_startframe_SPI();
	for (int i = 0; i<4; i++)
	{
		LED_strip_array[i][0] = Status_Brightness[i];
		LED_strip_array[i][1] = Status_Blue[i];
		LED_strip_array[i][2] = Status_Green[i];
		LED_strip_array[i][3] = Status_Red[i];
	}
	LED_update();
}