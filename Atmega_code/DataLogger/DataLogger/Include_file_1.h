/*
 * IncFile1.h
 *
 * Created: 11/03/2019 14.10.03
 *  Author: FP
 */ 


#ifndef INCLUDE_FILE_1_H
#define INCLUDE_FILE_1_H

// Macros and libraries
#define F_CPU 16000000UL //use 8MHz clock
#define BAUD 9600 //BAUD Rate for UART
#define LED_count 10 //Number of LED's used

#include <stdio.h> // Needed for printf + scanf
#include <avr/io.h> // avr macros
#include "Library/STDIO_UART.h" // Needed for stdio to UART redirection
#include <stdbool.h> // Needed for bool
#include <avr/interrupt.h> // Needed for interrupts
#include <string.h>
#include <avr/io.h>
#include <util/delay.h>
//#include <util/delay.h>

//APA_SPI.c Prototypes
void setup_SPI(void);
void send_colour_SPI(char,char,char,char,int);
void send_startframe_SPI(void);
void send_endframe_SPI(void);
void startup_Led(void);
void setup_LED_strip_array(void);
void LED_update(void);
void Plant_LED_colour_change(char, char, char);
void reset_Status_LED(void);
void update_status_LEDs(void);
void recieved_UART_LED(void);
void successful_UART_LED(void);
void failed_UART_LED(void);

// main.c prototypes
bool readLed(char led);
void ledOn(char led);
void ledOff(char led);
void toggleLed(char led);
void initPort(char DataDirReg, char BitNo);
void enablePullup(char DataReg, char BitNo);
void setupTimer0();
void setupADC(char adcbit);
char startConversion();
void promptReady();

//APA_SPI.c variables
char Blue_value;
char Green_value;
char Red_value;
char Brightness;
int LED_amount;
int boot_counter;
char LED_strip_array[LED_count][4];
char LED_single_array[4];
char Status_Brightness[4];
char Status_Blue[4];
char Status_Green[4];
char Status_Red[4];

#endif /* INCLUDE_FILE_1_H */