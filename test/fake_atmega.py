#!/usr/bin/env python3
import serial

serial_cfg = {  "dev": "./fake_atmega",
                "baud": 9600 }

ping_pong = {
    'led1on': "led1=1", 
    'led1off': "led1=0",
    'getadcval': "adcvalue",
    'getbtn1': "btn1 = something",
    'default': "I'm sorry I can't do that Dave (or anything else)",
    }

def do_ping_pong( command, reply_list ):
    try:
        return reply_list[command]
    except KeyError:
        return "{}: {}".format(reply_list['default'], repr(command))


if __name__ == "__main__":
    print( "Running port {}".format( serial_cfg['dev'] ) )

    with serial.Serial(serial_cfg['dev'], serial_cfg['baud'], timeout=1) as ser:
        try:
            while( True):
                command = ser.readline()   # read a '\n' terminated line
                if len(command) == 0: # ignore empty stuff
                    continue
                print( "Command received: {}".format(command ))
                reply = do_ping_pong( command.decode().strip(), ping_pong )
                print( "- sending reply: {}".format(reply ))
                ser.write( "{}\r\n".format(reply).encode())
        except KeyboardInterrupt:
            print('interrupted!')
