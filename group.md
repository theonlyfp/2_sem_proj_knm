Group: KNM
-----------------

Members:

* Kenneth Bjerg huzo
* Maximillan Wassmann Gregersen theonlyfp
* Nicolai Lyngs Jensen koldbaek

Juniper router: 

* Management 10.217.19.216:22
* External ip: 10.217.19.216

Raspberry 

* SSH access: 10.217.19.216:5001
* REST API access: [http://10.217.19.216:5010](http://10.217.19.216:5010)

Group web server: 

* SSH access: 10.217.16.62:22
* Dashboard access: [http://10.217.16.62:80](http://10.217.16.62:80)
 

