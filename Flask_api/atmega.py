#!/usr/bin/env python3

import serial
from time import sleep

def read_ser_line(serial_connection):
    reply = serial_connection.readline().decode()
    return reply.strip()

def read_temp(serial_connection):
    serial_connection.write("getadcval\n".encode())
    reply = read_ser_line(serial_connection)
    reply = adc_con_func(int(reply))
    return reply

def read_button(serial_connection):
    serial_connection.write("getbtn1\n".encode())
    reply = read_ser_line(serial_connection)
    return reply

def turn_on_LED(serial_connection):
    serial_connection.write("led1on\n".encode())
    reply = read_ser_line(serial_connection)
    return reply

def turn_off_LED(serial_connection):
    serial_connection.write("led1off\n".encode())
    reply = read_ser_line(serial_connection)
    return reply

def blink_LED(serial_connection):
    for i in range (10):
        reply = turn_on_LED(serial_connection);
        sleep(0.8)
        reply = turn_off_LED(serial_connection);
        sleep(0.8)
    return reply

def adc_con_func(adcvalue):
    multiplied_adcval = adcvalue*5
    part_adcval = multiplied_adcval/1024
    temp = (part_adcval-0.5)*100
    return temp

def UART_failure(serial_connection):
    serial_connection.write("UART_fail\n".encode())
    return

def UART_success(serial_connection):
    serial_connection.write("UART_success\n".encode())
    return
