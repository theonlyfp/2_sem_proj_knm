#!flask/bin/python3
from flask import Flask, request, jsonify
from flask_restful import reqparse, abort, Api, Resource
from flask_cors import CORS, cross_origin
from atmega import read_temp, read_button, turn_on_LED, turn_off_LED, blink_LED, UART_failure, UART_success
import serial

app = Flask(__name__)
CORS(app, resources=r'/*') # to allow external resources to fetch data
api = Api(app)

if __name__ == '__main__':
    serial_cfg = {  
        "dev": "/dev/serial0",
        "baud": 9600,
    }

    led_state_dict = {
    "on":turn_on_LED,
    "off":turn_off_LED,
    "blink":blink_LED,
    }

    UART_answer_list = ["led1=1","led1=0","btn1=1","btn1=0"]

@api.resource("/")
class url_index(Resource):
    def get(self):
        with serial.Serial(serial_cfg['dev'], serial_cfg['baud'], timeout=1) as serial_connection:
            temp_value=str(read_temp(serial_connection))
            button_state=str(read_button(serial_connection))
            if button_state not in UART_answer_list:
                UART_failure(serial_connection)
            else:
                UART_success(serial_connection)
            return jsonify(
                TEMPERATURE=temp_value,
                Button=button_state,
                )

@api.resource("/TEMPERATURE")
class url_temperature(Resource):
    def get(self):
        with serial.Serial(serial_cfg['dev'], serial_cfg['baud'], timeout=1) as serial_connection:
            temp_value=str(read_temp(serial_connection))
            UART_success(serial_connection)
            return jsonify(
                TEMPERATURE=temp_value
                )

@api.resource("/<led1>")
class url_led_toggle(Resource):
    def get(self, led1):
        return jsonify(
            LED=led1
            )

    def put(self,led1):
        state = request.json['state']
        with serial.Serial(serial_cfg['dev'], serial_cfg['baud'], timeout=1) as serial_connection:
            reply = led_state_dict[state](serial_connection)
            if reply not in UART_answer_list:
                UART_failure(serial_connection)
            else:
                UART_success(serial_connection)
            return(reply)

if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)